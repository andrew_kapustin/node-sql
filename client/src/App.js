import React, { useState } from 'react'
import './App.css';
import axios from 'axios'

function App() {

    const [name, setName] = useState("")
    const [age, setAge] = useState(0)
    const [country, setCountry] = useState("")
    const [position, setPosition] = useState("")
    const [wage, setWage] = useState(0)
    
    const [employeeList, setEmployeeList] = useState([])


    const addEmployee = () => {
        axios.post('http://localhost:3001/create', {
            name, 
            age, 
            country, 
            position, 
            wage
        }).then( ()=>{
            console.log('success');
        } )
    }

    const getEmployees = () => {
        axios.get('http://localhost:3001/employees')
            .then( (response)=> {
                setEmployeeList(response.data)
            })
    }


    return (
        <div className="App">
            <div className="information">
                <label >Name:</label>
                <input type="text" onChange={ (e) => {setName(e.target.value)} } />

                <label >Age:</label>
                <input type="number" onChange={ (e) => {setAge(e.target.value)} } />

                <label >Country:</label>
                <input type="text" onChange={ (e) => {setCountry(e.target.value)} } />

                <label >Position:</label>
                <input type="text" onChange={ (e) => {setPosition(e.target.value)} } />

                <label >Wage (year):</label>
                <input type="number" onChange={ (e) => {setWage(e.target.value)} } />

                <button onClick={addEmployee}>Add Employee</button>
            </div>

            <div className="employees">
                <button onClick={getEmployees}>Show employees</button>
            </div>

            {employeeList.map( (val, key) => {
                return <div className='employee'> 
                    <h3>{val.name}</h3> 
                    <h3>{val.age}</h3> 
                    <h3>{val.country}</h3> 
                    <h3>{val.position}</h3> 
                    <h3>{val.wage}</h3> 
                </div>
            } )}
        </div>
    );
}

export default App;
